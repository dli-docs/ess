=== Interim Review

The interim review of the appraisal plan of an employee is done by the main appraiser of the employee.
The first approver in the workflow is mentioned as the main appraiser.

To complete the interim review, follow the steps below.

1. Go to *Performance Management* > *Employee Appraisal* > *Initiate Review*.
+
The *Interim Review* page displays.
The initiated appraisal plan will be listed in the *Appraisal Plan for Interim Review grid* in the *Interim Review page*.

2. Click the *Settings* icon near the employee code and select *Create Interim Review* in the *Appraisal Plan for Interim Review grid*.
+

image:pmm/pmm-image10.png[]

3. Verify the appraisal plan details, objective assessment, core values and competency assessment and formal training needs.

4. In the *Objective Assessment and Core* value and competency assessment, click the *Edit* icon to add the *Interim Review Comments*.

5. Click the *+* button and type the comments in the comment field.

6. Click the *Update* button to save the changes.

7. After verifying the appraisal plan, the main appraiser can do any of the following.
+
* *Submit* *the* *appraisal* *plan*.
+
** If the main appraiser is satisfied with the appraisal plan submitted by the employee, he/she can click the *Submit* button.
** Then the appraisal plan can be reviewed by the next approver.
** The submitted record will be listed in the *Interim Review Against Appraisal Plan grid* in the *Interim Review Page* in Pending Approval Status.

* *Return to the employee*
+
** If the main appraiser is not satisfied with the details provided by the employee or need any clarifications, he/she can click the *Return to employee name* button.
** Then the appraisal plan will be return to the particular employee and he/she can make necessary changes and resend the appraisal plan to the main appraiser.

* *Save*
+
** The main appraiser can save the appraisal plan by clicking the *Save* button.
** Then the record will be listed in the *Interim Review Against Appraisal Plan grid* in the *Interim Review Page*.
