= Feedback Registration
Adarsh BM <adarsh@dli-pdc.com>
3.0, July 29, 2022: AsciiDoc article template
:icons: font
:sectnums:
:page-category: guide

[NOTE.summary,caption=" "]
====
The _Feedback Registration_ transaction allows employees to submit feedbacks, complaints, or suggestions to their managers. Managers can review and take actions on the complaints and suggestions they receive and also reward the employees for their suggestions and ideas.
====

== Submit feedbacks, suggestions or complaints

[NOTE]
====
If you are a proxy user and want to submit suggestions, feedbacks or complaints on behalf of another employee, refer to the *Creating Feedback Registration Request (Proxy)* section.
====

To submit feedbacks, suggestions or complaints, follow the steps below.

. Go to *Feedback and Clearance* and select *Feedback Registration*.
+
image:feedback-registration-request/image1.png[]
+
The *Feedback Registration Application* page opens.
+
. On the *Request* tab, click the *Create New* button in the top right corner and then select any of the following types of requests:
.. *Suggestions*: To submit suggestions, select this.
.. *Complaints*: To submit complaints, select this.
+
image:feedback-registration-request/image2.png[]
+
Based on your selection, the *Feedback Registration* form appears in either suggestion or complaints mode.
+
image:feedback-registration-request/image3.png[]
+
. Provide the following details in the respective field.
+
[width="99%",cols="30,79",options="header"]
|===
|If
|Then

|Suggestions or feedbacks
a|. In the *Suggestion* field, type your suggestions.
. In the *Solution* field, type your solution. Use this if you want to provide solution to any problems or challenges.
. In the comment field, type your comments.
. To attach any documents, do the following:
.. Click the *Browse* button.
.. Browse, select the file, and then select *Open*.
.. Click the *Attach* button.
. The *Approver Solution* field will be made editable only for approver to enter the solution if any.
. The *Reward Applicable* drop-down will be enabled only for the Approvers to enter if the employee deserves any rewards or not.
|Complaints
a|In the *Current Complaint* field, type your complaint description.

. In the *Current Complaint* field, type your complaint description.
. In the *Comments* field, type your comments.
. To attach any documents, do the following:
.. Click the *Browse* button.
.. Browse, select the file, and then select *Open*.
.. Click the *Attach* button.
The *Approver Solution* field will be made editable only for approver to enter solution if any.

|===
. Once you have completed filling the Feedback Registration form, you can save, submit or close the Feedback Registration as required.
+
[width="99%",cols="26,73",options="header"]
|===
|If
|Then

|To save the form for later editing.
a|Click the *Save* button.

You can view and open the saved _Feedback Registration_ transactions on the *Feedback Registration* page > *Request* tab > *Feedback Registration List* grid.

a|To submit the form for review and approval.

a|Click the *Submit* button.

** You can view and track the status of the submitted transaction on the *Feedback Registration List* grid (*Feedback Registration* page >> *Request* tab).

[NOTE]
====
* When you submit a Feedback Registration form, it goes through an approval workflow where each level of approvers can either approve or reject your request.

* To view the approval workflow applicable to the current transaction, click the *Options* slide-button on the right side of the form.

* Your transaction remains in the _Pending Approval_ status, and it changes to _Approved_ status after the final-level approver approves it.
====

a| To close the form or cancel the transaction.

|Click the *Close* button.
|===

== Submit/ delete a saved feedback registration transaction

. On the Feedback Registration page, click the *Request* tab and then navigate to the *Feedback Registration List grid*
. Do any of the following as required
+
[width="99%",cols="37,77",options="header"]
|===
|If
|Then

|To edit, submit or delete any saved request individually.
a|. Locate the saved _Feedback Registration_ request you want to edit or submit.
[%hardbreaks]
. Click the gear icon to the left of the feedback registration transaction you want to edit/ submit and then select *Open*.
+
The *Feedback Registration* request form opens.

. On the _Feedback Registration_ instance form make the required changes if any and then click any of the following buttons as required:
+
* To submit the request, click the *Submit* button.
* To delete the request, click the *Delete* button. Deleted request will no longer be shown in the system.

|To submit or delete more than one saved request in bulk.
a|. From the *Bulk Actions* drop-down list, select any option below as required:
+
* To submit the request, click the *Submit* button.
* To delete the request, click the *Delete* button. Deleted request will no longer be shown in the system.
. Select the Feedback Registration transaction you want to submit/ delete.
. Click the *Apply* button.

|===

